from xsstool.cli import XsstoolCli
from xsstool.XsstoolConfig import XsstoolConfig
import logging

logging.basicConfig(
    format='%(levelname)s: %(message)s',
    filename='xsstool.log',
    level=logging.DEBUG,
    filemode='w'
)
XsstoolConfig.load('vulnsiteauth.conf')
XsstoolCli().cmdloop()
