To try xsstool, please clone vulnsite repo:
https://gitlab.com/darknyaz/vulnsite

To run vulnsite do next steps:

0. You need flask installed and FLASK_APP environment variable set to 
   "vulnsite\\\_\_init\_\_.py" (relative to vulnsite local repo)

1. Init DB by executing "flask init-db".

2. Execute "flask run" with --host option set to "0.0.0.0" (it is necessary
   because Selenium does not send requests that are goes to localhost).

3. Create user on site.


To run xsstool:

0. Install all dependencies: selenium, beautifulsoup4, etc.

1. Run xsstool.py, setup target and auth. To help use ?, help <command> or
   view. In short: to enter target menu enter "target", to set ulr type
   "set url http://10.0.0.1:5000". The same for auth. Parameters to set you
   can find in profile/vulnsiteauth.conf. This config loads by default now.
   Target must not be localhost! (read above)

2. Enter "start" in root CLI. Drink something while working.

3. Check out xsstool.log for string "VULNERABILITY WAS FOUND".
