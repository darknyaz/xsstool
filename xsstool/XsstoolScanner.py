from .XsstoolSpider import XsstoolSpider
from .XsstoolProxy import XsstoolProxy
from .XsstoolSpider import ActionOnSiteType
from .XsstoolConfig import XsstoolConfig
from .XsstoolExtras import XsstoolExtras
from selenium import webdriver
from selenium.webdriver.common.proxy import Proxy, ProxyType
from json import load as loadjson
from urllib.parse import urlencode
from threading import Thread
import re
import logging

class PayloadScenario:
    def __init__(
        self,
        scenario,
        payload_point,
        payload,
        payload_rand_str,
        payload_res
    ):
        self.scenario = scenario
        self.payload_point = payload_point
        self.payload = payload
        self.payload_rand_str = payload_rand_str
        self.payload_res = payload_res


class XsstoolScanner:
    def __init__(self):
        self.proxy = None
        self.driver = None
        self.payloads = {}
        with open('payloads.conf', 'r') as f:
            self.payloads = loadjson(f)
        self.payload_scenarios = []
        self.current_payload_scenario = None
        self.clicked_links = []
    
    def start(self):
        spider = XsstoolSpider()
        spider.start()
        self.init_payload_scenarios(spider.scenarios)

        self.proxy = XsstoolProxy()
        p_thread = Thread(target=self.proxy.start)
        p_thread.setDaemon(True)
        p_thread.start()

        prox = Proxy()
        prox.proxy_type = ProxyType.MANUAL
        prox.http_proxy = XsstoolExtras.socket_url(self.proxy.server_socket)
        capabilities = webdriver.DesiredCapabilities.FIREFOX
        prox.add_to_capabilities(capabilities)

        self.driver = webdriver.Firefox(desired_capabilities=capabilities)
        self.driver.implicitly_wait(3)

        # PROXY = XsstoolExtras.socket_url(self.proxy.server_socket)

        # chrome_options = webdriver.ChromeOptions()
        # chrome_options.add_argument('--proxy-server=%s' % PROXY)
        # chrome_options.add_argument('--no-sandbox')

        # self.driver = webdriver.Chrome(options=chrome_options)
        # self.driver.implicitly_wait(3)

        for payload_scenario in self.payload_scenarios:
            self.execute_payload_scenario(payload_scenario)

        XsstoolExtras.get_page_skip_alerts(
            self.driver,
            XsstoolConfig.get(['target', 'url'])
        )
        self.check_vulns()

        self.proxy.stop()
        self.driver.close()

    def execute_payload_scenario(self, payload_scenario):
        self.current_payload_scenario = payload_scenario

        for action in \
            payload_scenario.scenario[:payload_scenario.payload_point[0]]:
            XsstoolExtras.execute_site_action(self.driver, action)
        
        callback_re = ''
        action = payload_scenario.scenario[payload_scenario.payload_point[0]]
        if action.type == ActionOnSiteType.CLICK_LINK:
            callback_re = r'GET\s+' + \
                re.escape(action.elem_soup.find().get('href')) + \
                r'\s+HTTP'
        else:
            params = action.post_params
            if not params:
                params = action.get_params
            callback_re = re.escape(urlencode(params))
        self.proxy.register_oneoff_regex_req_callback(
            callback_re,
            self.payloader_proxy_callback
        )
        XsstoolExtras.execute_site_action(
            self.driver,
            action
        )

    def payloader_proxy_callback(self, req):
        req_str = req.decode('iso-8859-1')

        current_action = self.current_payload_scenario.scenario[
            self.current_payload_scenario.payload_point[0]
        ]
        payload_param = self.current_payload_scenario.payload_point[1]
        param_value = current_action.post_params.get(payload_param)
        if not param_value:
            param_value = current_action.get_params.get(payload_param)
        current_payload = self.current_payload_scenario.payload
        payload = current_payload[1]['prefix'] +\
            self.current_payload_scenario.payload_rand_str +\
            current_payload[1]['suffix']

        regex = payload_param + '=' + param_value
        regex_sub = payload_param + '=' + payload
        req_str = re.sub(regex, regex_sub, req_str)

        if req_str[:4] == 'POST':
            content_length_re = r'Content-Length:\s+(?P<len>\d+?)\s*\n'
            content_length = int(
                re.search(content_length_re, req_str).group('len')
            )
            content_length += len(current_payload[1]['prefix']) +\
                len(current_payload[1]['suffix'])
            req_str = re.sub(
                content_length_re,
                r'Content-Length: ' + str(content_length) + '\r\n',
                req_str
            )

        return req_str.encode('iso-8859-1')

    def init_payload_scenarios(self, scenarios):
        for scenario in scenarios:
            scenario_is_payloadable = False
            for action in scenario:
                if action.get_params or action.post_params:
                    scenario_is_payloadable = True
                    break
            if scenario_is_payloadable:
                payload_points = self.get_payload_points(scenario)
                for payload_point in payload_points:
                    for payload_name, payload in self.payloads.items():
                        rand_data = XsstoolExtras.gen_random_form_data()
                        self.payload_scenarios.append(PayloadScenario(
                            scenario,
                            payload_point,
                            (payload_name, payload),
                            rand_data,
                            rand_data
                        ))

    def get_payload_points(self, scenario):
        result = []
        for i in range(len(scenario)):
            action = scenario[i]
            for param in action.get_params:
                result.append((i, param))
            for param in action.post_params:
                result.append((i, param))
        return result

    def check_vulns(self):
        from time import sleep
        
        while XsstoolExtras.alert_is_present(self.driver):
            alert_text = self.driver.switch_to_alert().text

            for payload_scenario in self.payload_scenarios:
                if payload_scenario.payload_res == alert_text:
                    logging.info('VULNERABILITY WAS FOUND')
                    logging.info('------------------------------')
                    logging.info('SCENARIO')
                    logging.info('--------')
                    for action in payload_scenario.scenario:
                        logging.info('')
                        logging.info(str(action))
                    logging.info('')
                    logging.info('--------')
                    logging.info(
                        'VULNERABLE ACTION NUMBER: ' + 
                        str(payload_scenario.payload_point[0])
                    )
                    logging.info(
                        'VULNERABLE PARAMETER: ' + 
                        str(payload_scenario.payload_point[1])
                    )
                    logging.info(
                        'PAYLOAD NAME: ' + 
                        str(payload_scenario.payload[0])
                    )
                    logging.info(
                        'PAYLOAD: ' + 
                        str(payload_scenario.payload[1]['prefix']) +
                        str(payload_scenario.payload_rand_str) +
                        str(payload_scenario.payload[1]['suffix'])
                    )
                    logging.info('------------------------------')

                    break

            self.driver.switch_to_alert().accept()
            sleep(1)

        links = self.driver.find_elements_by_css_selector('a')
        i = 0

        while i < len(links):
            ln = links[i]
            href = ln.get_attribute('href')

            if href not in self.clicked_links:
                ln.click()
                self.clicked_links.append(href)

                self.check_vulns()
                
                links = self.driver.find_elements_by_css_selector('a')
                i = 0
                
                continue

            i += 1

        XsstoolExtras.get_back_skip_alerts(self.driver)
            