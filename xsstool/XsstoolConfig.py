import json
import logging
import os.path

class XsstoolConfig:
    profiles_dirname = 'profiles'
    config = {}

    @staticmethod
    def get(option):
        dct = XsstoolConfig.config
        for key in option[:-1]:
            dct = dct[key]
        return dct[option[-1]]

    @staticmethod
    def set(option, value):
        dct = XsstoolConfig.config
        for key in option[:-1]:
            dct = dct[key]
        dct[option[-1]] = value

    @staticmethod
    def load(filename):
        from .XsstoolExtras import XsstoolExtras

        if not XsstoolExtras.dir_exists(XsstoolConfig.profiles_dirname):
            logging.error('Profiles directory not found.')
            return
        
        filepath = os.path.join(XsstoolConfig.profiles_dirname, filename)
        if not XsstoolExtras.file_exists(filepath):
            logging.error(filename + ' profile not found.')
            return

        try:
            with open(filepath, 'r') as f:
                XsstoolConfig.config = json.load(f)
        except EnvironmentError as e:
            logging.error('Error occurs while loading ' + filename + 
                ' profile: ' + str(e))

    @staticmethod
    def save(filename):
        from .XsstoolExtras import XsstoolExtras

        if not XsstoolExtras.dir_exists(XsstoolConfig.profiles_dirname):
            logging.error('Profiles directory not found.')
            return

        try:
            with open(
                os.path.join(XsstoolConfig.profiles_dirname, filename),
                'w'
            ) as f:
                json.dump(XsstoolConfig.config, f)
        except EnvironmentError as e:
            logging.error('Error occurs while saving ' + filename + 
                ' profile: ' + str(e))

    