from bs4 import BeautifulSoup
import os.path
import re
from urllib import parse
from enum import Enum
from random import randrange
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
import logging


ActionOnSiteType = Enum(
    'ActionOnSiteType',
    'ENTER_SITE AUTH_ENTER_SITE CLICK_LINK SUBMIT_FORM SUBMIT_ONPAGE_FORM'
)


class ActionOnSite:
    def __init__(self,
        type, 
        page_url = '',
        elem_soup = '',
        req = b'',
        post_params = {}
    ):
        self.type = type
        self.page_url = page_url
        self.elem_soup = elem_soup
        self.req = req
        self.get_params = XsstoolExtras.extract_get_param_values(req)
        self.post_params = XsstoolExtras.extract_post_param_values(req)

    def __str__(self):
        result = 'Action type: ' + str(self.type) + \
            '\nPage: ' + str(self.page_url) + \
            '\nSoup: ' + str(self.elem_soup) + \
            '\nRequest: ' + self.req.decode('iso-8859-1')
        if self.type == ActionOnSiteType.CLICK_LINK:
            result += '\nGet params:\n' + str(self.get_params)
        else:
            result += '\nPost params:\n' + str(self.post_params)
        return result


class XsstoolExtras:
    @staticmethod
    def dir_exists(dirpath):
        return os.path.exists(dirpath) and os.path.isdir(dirpath)

    @staticmethod
    def file_exists(filepath):
        return os.path.exists(filepath) and os.path.isfile(filepath)

    @staticmethod
    def socket_url(socket):
        socketname = socket.getsockname()
        return socketname[0] + ':' + str(socketname[1])

    @staticmethod
    def gen_random_form_data():
        return 'xsstool' + str(hex(randrange(0x100000000))[2:]).zfill(8)

    @staticmethod
    def extract_get_param_values(http_req):
        if not http_req:
            return {}
        http_req_str = http_req.decode('iso-8859-1')
        req_lines = re.split(r'\s*\n|\s*\r\n', http_req_str)
        url = re.search(
            r'^(GET|POST)\s+(?P<url>.*?)\s+HTTP.*$',
            req_lines[0]
        ).group('url')
        return dict(parse.parse_qsl(parse.urlsplit(url).query))

    @staticmethod
    def extract_post_param_values(http_req):
        if not http_req:
            return {}
        http_req_str = http_req.decode('iso-8859-1')
        req_lines = re.split(r'\s*\n|\s*\r\n', http_req_str)
        if req_lines[0][:4] != 'POST':
            return {}
        return dict(parse.parse_qsl(req_lines[-1]))

    @staticmethod
    def extract_host_port(http_req):
        http_req_str = http_req.decode('iso-8859-1')
        req_lines = re.split(r'\s*\n|\s*\r\n', http_req_str)
        host_line = [l for l in req_lines if l[:5] == "Host:"][0]
        m = re.search(r'^Host:\s+(?P<host>.*?)(:(?P<port>.*?))?\s*$', host_line)
        host = m.group('host')
        port = m.group('port')
        if not port:
            port = '80'
        port = int(port)
        return host, port

    @staticmethod
    def wait_until_page_loaded_skip_alerts(driver):
        XsstoolExtras.skip_alerts(driver)
        XsstoolExtras.wait_until_page_loaded(driver)

    @staticmethod
    def wait_until_page_loaded(driver):
        try:
            WebDriverWait(driver, 3).until(
                lambda d : 
                    d.execute_script('return document.readyState') == 'complete'
            )
        except TimeoutException as e:
            logging.error('Timeout loading: ' + str(e))
            raise e

    @staticmethod
    def get_page_skip_alerts(driver, url):
        driver.get(url)
        XsstoolExtras.wait_until_page_loaded_skip_alerts(driver)

    @staticmethod
    def get_page(driver, url):
        driver.get(url)
        XsstoolExtras.wait_until_page_loaded(driver)

    @staticmethod
    def get_back_skip_alerts(driver):
        driver.back()
        XsstoolExtras.wait_until_page_loaded_skip_alerts(driver)
            
    @staticmethod
    def auth(driver):
        from .XsstoolConfig import XsstoolConfig

        XsstoolExtras.skip_alerts(driver)

        if XsstoolConfig.get(['auth', 'enabled']) == 'true':
            XsstoolExtras.get_page_skip_alerts(
                driver,
                XsstoolConfig.get(['target', 'url'])
            )

            if XsstoolConfig.get(['auth', 'use_old_session_cookie']) == 'true':
                domain = re.search(
                    r'http://(?P<domain>.*?)(:|/).*',
                    XsstoolConfig.get(['target', 'url'])
                ).group('domain')
                driver.add_cookie({
                    'name' : XsstoolConfig.get(['auth', 'session_cookie_name']),
                    'value' : XsstoolConfig.get(['auth', 'session_cookie']),
                    'domain' : domain
                })
            else:
                XsstoolExtras.get_page_skip_alerts(
                    driver,
                    XsstoolConfig.get(['auth', 'url'])
                )
                username_input = driver.find_element_by_name(
                    XsstoolConfig.get(['auth', 'username_field_name'])
                )
                username_input.send_keys(
                    XsstoolConfig.get(['auth', 'username'])
                )
                password_input = driver.find_element_by_name(
                    XsstoolConfig.get(['auth', 'password_field_name'])
                )
                password_input.send_keys(
                    XsstoolConfig.get(['auth', 'password'])
                )
                username_input.find_element_by_xpath('..')\
                    .find_element_by_xpath('//input[@type=\'submit\']')\
                    .click()
                XsstoolExtras.wait_until_page_loaded_skip_alerts(driver)

    @staticmethod
    def execute_site_action(driver, action):
        from .XsstoolConfig import XsstoolConfig

        if action.type == ActionOnSiteType.ENTER_SITE:
            XsstoolExtras.get_page_skip_alerts(
                driver,
                XsstoolConfig.get(['target', 'url'])
            )
        elif action.type == ActionOnSiteType.AUTH_ENTER_SITE:
            XsstoolExtras.auth(driver)
        elif action.type == ActionOnSiteType.CLICK_LINK:
            links = driver.find_elements_by_css_selector('a')
            for ln in links:
                ln_soup = BeautifulSoup(
                    ln.get_attribute('outerHTML'),
                    'html.parser'
                )
                if str(action.elem_soup) == str(ln_soup):
                    ln.click()
                    XsstoolExtras.wait_until_page_loaded_skip_alerts(driver)
                    return
        else:
            forms = driver.find_elements_by_css_selector('form')
            for form in forms:
                form_soup = BeautifulSoup(
                    form.get_attribute('outerHTML'),
                    'html.parser'
                )
                if str(action.elem_soup) == str(form_soup):
                    params = action.post_params
                    if not params:
                        params = action.get_params
                    form_inputs = \
                        form.find_elements_by_css_selector('input,textarea')
                    form_submit = \
                        form.find_element_by_xpath('//input[@type=\'submit\']')
                    nonamed_inputs_count = 0

                    for input in form_inputs:
                        if input.get_attribute('type') == 'submit':
                            continue
                        input_name = input.get_attribute('name')
                        if not input_name:
                            input_name = 'noname' + str(nonamed_inputs_count)
                            nonamed_inputs_count += 1
                        input.send_keys(params[input_name])
                    
                    form_submit.click()
                    XsstoolExtras.wait_until_page_loaded_skip_alerts(driver)
        
    @staticmethod
    def alert_is_present(driver):
        try:
            driver.switch_to_alert()
        except NoAlertPresentException:
            return False
        return True

    @staticmethod
    def skip_alerts(driver):
        from time import sleep
        while XsstoolExtras.alert_is_present(driver):
            driver.switch_to_alert().accept()
            sleep(1)


def docstring_parameter(*params):
    def dec(obj):
        obj.__doc__ = obj.__doc__.format(*params)
        return obj
    return dec
