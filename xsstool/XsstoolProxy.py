from .XsstoolExtras import XsstoolExtras
import socket
import re
from threading import Thread


class XsstoolProxy:
    def __init__(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind(('localhost', 54321))
        self.server_socket.listen(50)
        self.proxy_enabled = True
        self.callback_regex = None
        self.oneoff_regex_req_callback = None
    
    def start(self):
        while self.proxy_enabled:
            try:
                client_conn, client_addr = self.server_socket.accept()
            except OSError:
                break

            t = Thread(
                target = self.proxy_conn,
                args = (client_conn, client_addr)
            )
            t.setDaemon(True)
            t.start()

    def proxy_conn(self, client_conn, client_addr):
        request = receive_all(client_conn)

        if not request:
            return

        request = self.handle_callback(request)

        host, port = XsstoolExtras.extract_host_port(request)

        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((host, port))
        client_socket.setblocking(0)

        send_all_nonblocking(client_socket, request)
        response = receive_all_nonblocking(client_socket)

        client_socket.close()

        client_conn.sendall(response)

        client_conn.close()

    def stop(self):
        self.proxy_enabled = False
        self.server_socket.close()

    def register_oneoff_regex_req_callback(self, regex, callback):
        self.callback_regex = regex
        self.oneoff_regex_req_callback = callback

    def handle_callback(self, req):
        result = req
        if self.oneoff_regex_req_callback:
            req_str = req.decode('iso-8859-1')
            if re.search(self.callback_regex, req_str):
                result = self.oneoff_regex_req_callback(req)
                self.callback_regex = None
                self.oneoff_regex_req_callback = None
        return result


def receive_all(sock):
    SIZE = 1024
    data = b''
    while True:
        try:
            part = sock.recv(SIZE)
        except OSError:
            break
        data += part
        if len(part) < SIZE:
            break
    return data

def send_all_nonblocking(sock, data):
    sent_bytes = 0
    while sent_bytes < len(data):
        try:
            start = sent_bytes
            sent_bytes += sock.send(data[sent_bytes:])
        except socket.error:
            continue
        except OSError:
            break

def receive_all_nonblocking(sock):
    SIZE = 1024
    data = b''
    while True:
        try:
            part = sock.recv(SIZE)
        except socket.error:
            continue
        except OSError:
            break
        if not part:
            break
        data += part
    return data
