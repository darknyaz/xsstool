from .XsstoolBaseCli import XsstoolBaseCli
from xsstool.XsstoolExtras import docstring_parameter

class XsstoolScannerCli(XsstoolBaseCli):
    prefix = ['scanner']
    options = {}
    prefix_str = 'xsstool/' + '/'.join(prefix)
    intro = prefix_str + ' settings. ? to help.\n'
    prompt = '(' + prefix_str + ') '

    def __init__(self):
        XsstoolBaseCli.__init__(
            self,
            XsstoolScannerCli.prefix,
            XsstoolScannerCli.options
        )

    @docstring_parameter(prefix_str)
    def do_quit(self, args):
        """ Quit {0}. """
        return True
