from .XsstoolBaseCli import XsstoolBaseCli
from xsstool.XsstoolConfig import XsstoolConfig
from xsstool.XsstoolExtras import docstring_parameter

class XsstoolProfileCli(XsstoolBaseCli):
    prefix = ['profile']
    options = {}
    prefix_str = 'xsstool/' + '/'.join(prefix)
    intro = prefix_str + ' settings. ? to help.\n'
    prompt = '(' + prefix_str + ') '

    def __init__(self):
        XsstoolBaseCli.__init__(
            self,
            XsstoolProfileCli.prefix,
            XsstoolProfileCli.options
        )

    def do_load(self, args):
        """
            load [profile_name]
            Load profile.
        """
        splitted_args = args.split()

        if len(splitted_args) != 1:
            return
        
        XsstoolConfig.load(splitted_args[0])

    def do_save(self, args):
        """
            save [profile_name]
            Save profile.
        """
        splitted_args = args.split()

        if len(splitted_args) != 1:
            return
        
        XsstoolConfig.save(splitted_args[0])

    @docstring_parameter(prefix_str)
    def do_quit(self, args):
        """ Quit {0}. """
        return True
