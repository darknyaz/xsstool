from .XsstoolBaseCli import XsstoolBaseCli
from xsstool.XsstoolExtras import docstring_parameter

class XsstoolProxyCli(XsstoolBaseCli):
    prefix = ['proxy']
    options = {}
    prefix_str = 'xsstool/' + '/'.join(prefix)
    intro = prefix_str + ' settings. ? to help.\n'
    prompt = '(' + prefix_str + ') '

    def __init__(self):
        XsstoolBaseCli.__init__(
            self,
            XsstoolProxyCli.prefix,
            XsstoolProxyCli.options
        )

    @docstring_parameter(prefix_str)
    def do_quit(self, args):
        """ Quit {0}. """
        return True