import cmd
from .XsstoolBaseCli import XsstoolBaseCli
from .XsstoolTargetCli import XsstoolTargetCli
from .XsstoolAuthCli import XsstoolAuthCli
from .XsstoolProxyCli import XsstoolProxyCli
from .XsstoolSpiderCli import XsstoolSpiderCli
from .XsstoolScannerCli import XsstoolScannerCli
from .XsstoolProfileCli import XsstoolProfileCli
from xsstool.XsstoolScanner import XsstoolScanner

class XsstoolCli(XsstoolBaseCli):
    prefix = []
    options = {}
    intro = 'xsstool shell. ? to help.\n'
    prompt = '(xsstool) '

    def __init__(self):
        XsstoolBaseCli.__init__(
            self,
            XsstoolCli.prefix,
            XsstoolCli.options
        )
        self.target_cli = XsstoolTargetCli()
        self.auth_cli = XsstoolAuthCli()
        self.proxy_cli = XsstoolProxyCli()
        self.spider_cli = XsstoolSpiderCli()
        self.scanner_cli = XsstoolScannerCli()
        self.profile_cli = XsstoolProfileCli()

    def do_target(self, args):
        """ Target settings. """
        self.target_cli.cmdloop()

    def do_auth(self, args):
        """ Authentication settings. """
        self.auth_cli.cmdloop()

    def do_proxy(self, args):
        """ Proxy settings. """
        self.proxy_cli.cmdloop()

    def do_spider(self, args):
        """ Spider settings. """
        self.spider_cli.cmdloop()

    def do_scanner(self, args):
        """ Scanner settings. """
        self.scanner_cli.cmdloop()

    def do_profile(self, args):
        """ Profile settings. """
        self.profile_cli.cmdloop()

    def do_quit(self, args):
        """ Quit xsstool. """
        return True

    def do_start(self, args):
        """ Start xsstool. """
        scanner = XsstoolScanner()
        scanner.start()
