from .XsstoolBaseCli import XsstoolBaseCli
from xsstool.XsstoolExtras import docstring_parameter

class XsstoolSpiderCli(XsstoolBaseCli):
    prefix = ['spider']
    options = {}
    prefix_str = 'xsstool/' + '/'.join(prefix)
    intro = prefix_str + ' settings. ? to help.\n'
    prompt = '(' + prefix_str + ') '

    def __init__(self):
        XsstoolBaseCli.__init__(
            self,
            XsstoolSpiderCli.prefix,
            XsstoolSpiderCli.options
        )

    @docstring_parameter(prefix_str)
    def do_quit(self, args):
        """ Quit {0}. """
        return True
