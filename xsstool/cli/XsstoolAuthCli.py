from .XsstoolBaseCli import XsstoolBaseCli
from xsstool.XsstoolExtras import docstring_parameter

class XsstoolAuthCli(XsstoolBaseCli):
    prefix = ['auth']
    options = {
        'enabled', 'use_old_session_cookie', 'url', 'username_field_name',
        'password_field_name', 'username', 'password', 'session_cookie_name',
        'session_cookie', 'logout_url'
    }
    prefix_str = 'xsstool/' + '/'.join(prefix)
    intro = prefix_str + ' settings. ? to help.\n'
    prompt = '(' + prefix_str + ') '

    def __init__(self):
        XsstoolBaseCli.__init__(
            self,
            XsstoolAuthCli.prefix,
            XsstoolAuthCli.options,
        )

    @docstring_parameter(prefix_str)
    def do_quit(self, args):
        """ Quit {0}. """
        return True