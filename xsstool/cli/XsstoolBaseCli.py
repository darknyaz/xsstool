import cmd
from tabulate import tabulate
from xsstool.XsstoolConfig import XsstoolConfig


class XsstoolBaseCli(cmd.Cmd):
    def __init__(self, prefix, options):
        cmd.Cmd.__init__(self)
        self.options = options
        self.prefix = prefix

    def set_option(self, option, value):
        if option in self.options:
            XsstoolConfig.set(self.prefix + [option], value)
        pass
    
    def do_set(self, args):
        """
            set [option] [value]
            Set option.
        """
        splitted_args = args.split()
        if len(splitted_args) == 2:
            self.set_option(splitted_args[0], splitted_args[1])

    def do_view(self, args):
        """ View commands and options. """
        command_help = self.get_command_help_dict()
        print('Commands:')
        print(
            tabulate(
                map(list, command_help.items()),
                ['Command', 'Description'],
                tablefmt='grid')
        )
        if self.options:
            print('\nOptions:')
            print(
                tabulate(
                    map(list, XsstoolConfig.get(self.prefix).items()),
                    ['Option', 'Value'],
                    tablefmt='grid')
            )

    def get_command_help_dict(self):
        commands = [name[3:] for name in self.get_names() if name[:3] == 'do_']
        helps = [getattr(self, 'do_' + command).__doc__ for command in commands]
        return dict(zip(commands, helps))
