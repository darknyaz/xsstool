from selenium import webdriver
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.common.exceptions import StaleElementReferenceException
from .XsstoolProxy import XsstoolProxy
from .XsstoolExtras import ActionOnSiteType
from .XsstoolExtras import ActionOnSite
from .XsstoolExtras import XsstoolExtras
from .XsstoolConfig import XsstoolConfig
from threading import Thread
from bs4 import BeautifulSoup
import re
import logging
from urllib.parse import urlencode


class XsstoolSpider:
    def __init__(self):
        self.proxy = None
        self.scenario = []
        self.scenarios = []
        self.driver = None
        self.current_page_title = None
        self.callback_req = b''
        self.clicked_links = []
        self.submitted_forms = []

    def proxy_callback(self, req):
        self.callback_req = req
        return req

    def start(self):
        self.proxy = XsstoolProxy()
        p_thread = Thread(target=self.proxy.start)
        p_thread.setDaemon(True)
        p_thread.start()

        prox = Proxy()
        prox.proxy_type = ProxyType.MANUAL
        prox.http_proxy = XsstoolExtras.socket_url(self.proxy.server_socket)
        capabilities = webdriver.DesiredCapabilities.FIREFOX
        prox.add_to_capabilities(capabilities)

        self.driver = webdriver.Firefox(desired_capabilities=capabilities)
        self.driver.implicitly_wait(3)

        # PROXY = XsstoolExtras.socket_url(self.proxy.server_socket)

        # chrome_options = webdriver.ChromeOptions()
        # chrome_options.add_argument('--proxy-server=%s' % PROXY)
        # chrome_options.add_argument('--no-sandbox')

        # self.driver = webdriver.Chrome(options=chrome_options)
        # self.driver.implicitly_wait(3)

        XsstoolExtras.get_page_skip_alerts(
            self.driver,
            XsstoolConfig.get(['target', 'url'])
        )
        self.scenario.append(ActionOnSite(ActionOnSiteType.ENTER_SITE))
        self.traverse_links()
        self.scenario.pop()
        if XsstoolConfig.get(['auth', 'enabled']):
            XsstoolExtras.auth(self.driver)
            XsstoolExtras.get_page_skip_alerts(
                self.driver,
                XsstoolConfig.get(['target', 'url'])
            )
            self.scenario.append(ActionOnSite(ActionOnSiteType.AUTH_ENTER_SITE))
            self.traverse_links()
            XsstoolExtras.get_page_skip_alerts(
                self.driver,
                XsstoolConfig.get(['auth', 'logout_url'])
            )
            self.scenario.pop()
        self.scenario.append(ActionOnSite(ActionOnSiteType.ENTER_SITE))
        self.fill_forms()
        self.scenario.pop()
        if XsstoolConfig.get(['auth', 'enabled']):
            XsstoolExtras.auth(self.driver)
            XsstoolExtras.get_page_skip_alerts(
                self.driver,
                XsstoolConfig.get(['target', 'url'])
            )
            self.scenario.append(ActionOnSite(ActionOnSiteType.AUTH_ENTER_SITE))
            self.fill_forms()
            XsstoolExtras.get_page_skip_alerts(
                self.driver,
                XsstoolConfig.get(['auth', 'logout_url'])
            )
            self.scenario.pop()

        for scenario in self.scenarios:
            logging.debug('SCENARIO:')
            logging.debug('------------------------------')
            for action in scenario:
                logging.debug('')
                logging.debug(str(action))
            logging.debug('')
            logging.debug('------------------------------')

        self.driver.close()

        self.proxy.stop()

    def traverse_links(self):
        self.traverse_links_with_func(self.traverse_links)

    def traverse_links_with_func(self, func):
        links = self.driver.find_elements_by_css_selector('a')
        i = 0
        no_link_clicks = True
        #clicked_links = []

        while i < len(links):
            ln = links[i]
            href = ln.get_attribute('href')

            if href not in self.clicked_links:
                ln_soup = BeautifulSoup(
                    ln.get_attribute('outerHTML'),
                    'html.parser'
                )

                callback_re = r'GET\s+' + re.escape(href) + r'\s+HTTP'
                self.proxy.register_oneoff_regex_req_callback(
                    callback_re,
                    self.proxy_callback
                )
                ln.click()
                XsstoolExtras.wait_until_page_loaded_skip_alerts(self.driver)
                no_link_clicks = False
                #clicked_links.append(href)
                self.clicked_links.append(href)

                if self.callback_req:
                    self.scenario.append(ActionOnSite(
                        ActionOnSiteType.CLICK_LINK,
                        self.driver.current_url,
                        ln_soup,
                        self.callback_req
                    ))

                    func()
                    
                    self.scenario.pop()

                links = self.driver.find_elements_by_css_selector('a')
                i = 0
                
                continue

            i += 1

        if no_link_clicks:
            self.scenarios.append(list(self.scenario))

        XsstoolExtras.get_back_skip_alerts(self.driver)

    def continue_traverse_links(self, link):
        link_href = link.get_attribute('href')
        for action in self.scenario:
            if XsstoolConfig.get(['target', 'url']) == link_href or\
                XsstoolConfig.get(['auth', 'logout_url']) == link_href or\
                action.type == ActionOnSiteType.CLICK_LINK and\
                    action.page_url == link_href:
                return False
        return True

    def fill_forms(self):
        forms = self.driver.find_elements_by_css_selector('form')
        i = 0
        no_forms_submitted = True
        #submitted_forms = []

        while i < len(forms):
            form = forms[i]
            form_html = form.get_attribute('outerHTML')
            form_inputs = form.find_elements_by_css_selector('input,textarea')
            form_submit = \
                form.find_element_by_xpath('//input[@type=\'submit\']')

            if form_html not in self.submitted_forms and\
                len(form_inputs) and form_submit:
                options = {}
                nonamed_inputs_count = 0
                form_soup = BeautifulSoup(form_html, 'html.parser')

                for input in form_inputs:
                    if input.get_attribute('type') == 'submit':
                        continue
                    input_name = input.get_attribute('name')
                    if not input_name:
                        input_name = 'noname' + str(nonamed_inputs_count)
                        nonamed_inputs_count += 1
                    input_data = XsstoolExtras.gen_random_form_data()
                    options[input_name]= input_data
                    input.send_keys(input_data)
                self.remember_page()

                callback_re = re.escape(urlencode(options))
                self.proxy.register_oneoff_regex_req_callback(
                    callback_re,
                    self.proxy_callback
                )
                form_submit.click()
                XsstoolExtras.wait_until_page_loaded_skip_alerts(self.driver)
                no_forms_submitted = False
                #submitted_forms.append(form_html)
                self.submitted_forms.append(form_html)

                action_type = ActionOnSiteType.SUBMIT_ONPAGE_FORM
                if self.page_is_changed():
                    action_type = ActionOnSiteType.SUBMIT_FORM

                if self.callback_req:
                    self.scenario.append(ActionOnSite(
                        action_type,
                        self.driver.current_url,
                        form_soup,
                        self.callback_req,
                        options
                    ))

                    self.fill_forms()
                    
                    self.scenario.pop()

                forms = self.driver.find_elements_by_css_selector('form')
                i = 0

                continue

            i += 1

        if no_forms_submitted:
            self.traverse_links_with_func(self.fill_forms)
            self.scenarios.append(list(self.scenario))

        XsstoolExtras.get_back_skip_alerts(self.driver)

    def continue_fill_forms(self, form):
        form_html = str(BeautifulSoup(
            form.get_attribute('outerHTML'),
            'html.parser'
        ))
        current_url = self.driver.current_url
        for action in self.scenario:
            if action.page_url == current_url and\
                str(action.elem_soup) == form_html:
                return False
        return True

    def remember_page(self):
        self.current_page_title = \
            self.driver.find_element_by_css_selector('title')

    def page_is_changed(self):
        try:
            self.current_page_title.get_attribute('innerHTML')
        except StaleElementReferenceException:
            return True
        return False
